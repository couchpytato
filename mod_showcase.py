import gui, pygame, os, time, mod, cfg
from pygame.locals import *

class Module(mod.Module):
    def __init__(self, screen):
        mod.Module.__init__(self, screen)
        import playlist
        self.addloop('PlaylistUpdate', self._listupd)

    def keyhandler(self, event):
        if event.type == KEYDOWN:
            if event.key == K_LEFT: self._quit()
            if event.key == K_ESCAPE: self._quit()
    
    def _quit(self):
        cfg.modname = 'mainmenu'
        cfg.modcf = cfg.cf['modules'][cfg.modname]
        self.loadmodule = cfg.modname

    def _listupd(self):
        import playlist
        self.changebykey('PLAYLIST-5', playlist.PLAYLIST.track(True, -5))
        self.changebykey('PLAYLIST-4', playlist.PLAYLIST.track(True, -4))
        self.changebykey('PLAYLIST-3', playlist.PLAYLIST.track(True, -3))
        self.changebykey('PLAYLIST-2', playlist.PLAYLIST.track(True, -2))
        self.changebykey('PLAYLIST-1', playlist.PLAYLIST.track(True, -1))
        self.changebykey('PLAYLIST+1', playlist.PLAYLIST.track(True, 1))
        self.changebykey('PLAYLIST+2', playlist.PLAYLIST.track(True, 2))
        self.changebykey('PLAYLIST+3', playlist.PLAYLIST.track(True, 3))
        self.changebykey('PLAYLIST+4', playlist.PLAYLIST.track(True, 4))
        self.changebykey('PLAYLIST+5', playlist.PLAYLIST.track(True, 5))