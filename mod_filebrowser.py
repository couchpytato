import gui, pygame, os, time, mod, playlist, cfg, random
from pygame.locals import *

class Module(mod.Module):
    def __init__(self, screen):
        mod.Module.__init__(self, screen)

    def filllist(self):
        self.data = []
        try:
            self.gui['LIST'].liststart, self.gui['LIST'].selected = self.history[self.path]
        except:
            pass
        if self.filetypes == '*':
            self.data = os.listdir(self.path)
        else:
            for file in os.listdir(self.path):
                if os.path.isdir(os.path.join(self.path, file)):
                    self.data.append(file)
                else:
                    for ft in self.filetypes:
                        if file[-3:].lower() == ft:
                            self.data.append(file)
        self.data.sort(key=str.lower)
        if len(self.data) > 0:
            for file in self.data:
                self.gui['LIST'].add(file,os.path.join(self.path,file),self._action,self._prevtrig)
            self.gui['LIST'].makelist()
        else:
            self.path, d = os.path.split(self.path)
            self.depth -= 1
        
    def init(self):
        self.filetypes = cfg.modcf['filetypes']
        self.path = cfg.modcf['path']
        self.action = cfg.modcf['action']
        self._path = cfg.modcf['path']
        self.history = {}
        self.filllist()
        self.prevcnt = 0
        self.history[self.path] = 0,0
        self.addloop('previewimg', self._prevwait)
        self.showgeneralgui()
        self.depth = 0
        cfg.lastmodname = cfg.modname

    def keyhandler(self, event):
        if event.type == KEYDOWN:
            if event.key == K_DOWN: self.gui['LIST'].select_next()
            if event.key == K_UP: self.gui['LIST'].select_last()
            if event.key == K_PAGEDOWN: self.gui['LIST'].next_page()
            if event.key == K_RIGHT:
                if os.path.isdir(self.sel_path):
                    self._updprev()
                    self.history[self.path] = self.gui['LIST'].liststart, self.gui['LIST'].selected
                    self.gui['LIST'].clear()
                    self.path = self.sel_path
                    self.filllist()
                    self.depth += 1
            if event.key == K_LEFT:
                if self.depth > 0:
                    self._updprev()
                    self.gui['LIST'].clear()
                    self.path, d = os.path.split(self.path)
                    self.filllist()
                    self.depth -= 1
                else:
                    cfg.modname = 'mainmenu'
                    cfg.modcf = cfg.cf['modules'][cfg.modname]
                    self.loadmodule = 'mainmenu'
            if event.key == K_RETURN: 
                self.gui['LIST'].press()
            if event.key == K_y: playlist.PLAYLIST.super_add(self._path, self.filetypes)

    def joyhandler(self, joystick):
        tol0 = 0.2
        tol1 = tol0 - (tol0 * 2)
        if joystick.get_axis(0) > tol0 or joystick.get_axis(0) < tol1:
            if joystick.get_axis(0) > 0:
                pass
            elif joystick.get_axis(0) < 0:
                pass
        if joystick.get_axis(1) > tol0 or joystick.get_axis(1) < tol1:
            if joystick.get_axis(1) > 0:
                self.gui['LIST'].select_next()
            elif joystick.get_axis(1) < 0:
                self.gui['LIST'].select_last()

    def _nut(self, value=None):
        pass

    def _action(self, value):
        if self.action == 'PLAY_MUSIC':
            if os.path.isdir(value):
                path = value
                playlist.PLAYLIST.add_dir(path, self.filetypes)
            elif os.path.isfile(value):
                path, fil = os.path.split(value)
                playlist.PLAYLIST.add_dir(path, self.filetypes, self.gui['LIST'].liststart + self.gui['LIST'].selected)
        elif self.action == 'SHOW_PICTURE':
            print 'SHOW_PICTURE'
        else:
            # pygame stops. This was mainly necessary for ZSNES, which doesn't start with running
            # pygame display, also it saves a lot cpu-time for the launched application
            playlist.PLAYLIST.pause()
            # quitting the display
            # launching the application with filename parameters
            os.system(self.action + ' "' + value + '"')
            # bringing up the display again
            playlist.PLAYLIST.playpause()

    def _updprev(self):
        if os.path.isdir(self.previmage):
            inp = os.listdir(self.previmage)
            image = cfg.thpath('blank.png')
            imagepool = []
            for line in inp:
                if self._ispic(os.path.join(self.previmage, line)):
                    #image = os.path.join(self.previmage, line)
                    imagepool.append(os.path.join(self.previmage, line))
            if len(imagepool) > 0:
                num = random.randint(0, len(imagepool)-1)
                image = imagepool[num]
            self.changebykey('PREVIEW', image)
        elif self._ispic(self.previmage):
            self.changebykey('PREVIEW', self.previmage)
    
    def _ispic(self, filename):
        ispic = False
        if os.path.isfile(filename):
            ext = os.path.splitext(filename)[1].lower().lstrip('.')
	    if ext in self.filetypes
                ispic = True
        return ispic

    def _prevwait(self):
        self.prevcnt += 1
        if self.prevcnt == 15:
            self._updprev()
	
    def _prevtrig(self, value):
        self.sel_path = os.path.join(self.path, value)
        self.previmage = value
        self.prevcnt = 0