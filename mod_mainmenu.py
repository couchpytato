import gui, pygame, os, time, mod, cfg
from pygame.locals import *

class Module(mod.Module):
    def __init__(self, screen):
        mod.Module.__init__(self, screen)
    
    def init(self):
        i = 0
        for module in cfg.cf['modules'].values():
            if module['modfilename'] != 'mod_mainmenu':
                self.gui['LIST'].add(module['title'], cfg.cf['modules'].keys()[i], self.loadmod, self.showicon)
            i += 1
        self.gui['LIST'].makelist()
        self.showgeneralgui()
        cfg.lastmodname = cfg.modname
    
    def showicon(self, value):
        if os.path.isfile(cfg.thpath(cfg.cf['modules'][value]['prefix'] + '_icon.png')):
            self.changebykey('ICON', cfg.thpath(cfg.cf['modules'][value]['prefix'] + '_icon.png'))
        else:
            self.changebykey('ICON', cfg.thpath('blank.png'))

    def loadmod(self, value):
        cfg.modname = value
        cfg.modcf = cfg.cf['modules'][value]
        self.loadmodule = value

    def keyhandler(self, event):
        if event.type == KEYDOWN:
            if event.key == K_DOWN: self.gui['LIST'].select_next()
            if event.key == K_UP: self.gui['LIST'].select_last()
            if event.key == K_PAGEDOWN: self.gui['LIST'].next_page()
            if event.key == K_RETURN: self.gui['LIST'].press()
            if event.key == K_RIGHT: self.gui['LIST'].press()