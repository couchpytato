#!/usr/bin/env python

try:
    # trying to import 'psyco' which precompiles
    # the program in order to make it faster
    # [http://psyco.sourceforge.net/]
    import psyco
    psyco.full()
except ImportError:
    pass

import pygame, os, time, sys, configobj
from pygame.locals import *
sys.path.append(os.path.join(sys.path[0], "data"))

class couchpytato:
    def __init__(self):
        cf = configobj.ConfigObj(os.path.join(sys.path[0], 'couchpytato.cfg'))
        if cf['sound'] == 'pysonic':
            import pySonic
            w = pySonic.World()
        pygame.display.init()
        pygame.font.init()
        import cfg, playlist
        resx = int(cfg.th['resolution'][0])
        resy = int(cfg.th['resolution'][1])
        pygame.display.set_caption('couchpytato') 
        pygame.display.set_icon(pygame.image.load(os.path.join(sys.path[0], 'icon.png')))
        if cfg.cf['fullscreen'] == '1':
            screen = pygame.display.set_mode((resx, resy),pygame.FULLSCREEN)
        else:
            screen = pygame.display.set_mode((resx, resy))
        pygame.mouse.set_visible(False)
        pygame.event.set_blocked(MOUSEMOTION)
        pygame.key.set_repeat(200, 80)
        clock = pygame.time.Clock()

        joystick = None
        if pygame.joystick.get_init():
            if pygame.joystick.get_count() > 0:
                joystick = pygame.joystick.Joystick(0)
                joystick.init()
    
        mod = __import__('mod_mainmenu')
        pytato = mod.Module(screen)
        playlist.PLAYLIST.load()
        
        while 1:
            pytato.runloop()
            clock.tick(int(cfg.cf['fps']))
            for event in pygame.event.get((KEYDOWN, KEYUP, QUIT)):
                if event.type == QUIT:
                    self.quit()
                if pytato.loadmodule:
                    try:
                        mod = __import__(pytato.loadmodule)
                    except:
                        mod = __import__(cfg.cf['modules'][pytato.loadmodule]['modfilename'])
                    pytato = mod.Module(screen)
                    pytato.loadmodule = None
                pytato.keyhandler(event)
                pytato.generalkeyhandler(event)
            if joystick: pytato.joyhandler(joystick)
    
    def quit(self):
        import playlist
        playlist.PLAYLIST.save()
        pygame.quit()
        sys.exit()
        
couchpytato()
